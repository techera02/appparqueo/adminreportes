package app.parqueo.domain.persistence_ports;

import java.sql.Date;
import java.util.List;

import app.parqueo.domain.model.ReporteDeParqueos;

public interface AdminReportesPersistence {

	List<ReporteDeParqueos> reporteDeParqueos(Integer pagenumber, Integer pagesize, Integer idmunicipio,
			Date fechainicio, Integer indiceFechainicio,
			Date fechafin, Integer indiceFechafin);
}
