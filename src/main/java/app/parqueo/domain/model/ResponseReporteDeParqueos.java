package app.parqueo.domain.model;

import java.util.List;

public class ResponseReporteDeParqueos extends ResponseError{
	public List<ReporteDeParqueos> lista;

	public List<ReporteDeParqueos> getLista() {
		return lista;
	}

	public void setLista(List<ReporteDeParqueos> lista) {
		this.lista = lista;
	}
	
}
