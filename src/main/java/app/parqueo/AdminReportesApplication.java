package app.parqueo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminReportesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminReportesApplication.class, args);
	}

}
