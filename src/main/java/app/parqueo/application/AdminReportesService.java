package app.parqueo.application;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ReporteDeParqueos;
import app.parqueo.domain.persistence_ports.AdminReportesPersistence;

@Service
public class AdminReportesService {
	
	AdminReportesPersistence adminReportesPersistence;
	
	public AdminReportesService(AdminReportesPersistence adminReportesPersistence)
	{
		this.adminReportesPersistence = adminReportesPersistence;
	}
	
	public List<ReporteDeParqueos> reporteDeParqueos(Integer pagenumber, Integer pagesize, Integer idmunicipio,
			Date fechainicio, Integer indiceFechainicio,
			Date fechafin, Integer indiceFechafin){
		return this.adminReportesPersistence.reporteDeParqueos(pagenumber, pagesize, idmunicipio,fechainicio,
				indiceFechainicio,fechafin, indiceFechafin);
	}
}
