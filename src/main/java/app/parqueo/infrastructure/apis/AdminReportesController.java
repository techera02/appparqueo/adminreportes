package app.parqueo.infrastructure.apis;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import app.parqueo.application.AdminReportesService;
import app.parqueo.domain.model.ResponseReporteDeParqueos;

@RestController
@RequestMapping("/AdminReportes")
public class AdminReportesController {
	private final AdminReportesService adminReportesService;

	@Autowired
	public AdminReportesController(AdminReportesService adminReportesService) {
		this.adminReportesService = adminReportesService;
	}

	@GetMapping
	("/ReporteDeParqueos/{pagenumber}/{pagesize}/{idmunicipio}/{fechainicio}/{indicefechainicio}/{fechafin}/{indicefechaFin}")
	public ResponseEntity<ResponseReporteDeParqueos> reporteDeParqueos(
			@PathVariable("pagenumber") Integer pagenumber,
			@PathVariable("pagesize") Integer pagesize, 
			@PathVariable("idmunicipio") Integer idmunicipio,
			@PathVariable("fechainicio") Date fechainicio,
			@PathVariable("indicefechainicio") Integer indiceFechainicio,
			@PathVariable("fechafin") Date fechafin,
			@PathVariable("indicefechaFin") Integer indiceFechaFin) {

		ResponseReporteDeParqueos response = new ResponseReporteDeParqueos();
		response.setEstado(true);
		
		try {
			response.setLista(this.adminReportesService.reporteDeParqueos(pagenumber, pagesize, idmunicipio,
					fechainicio, indiceFechainicio, fechafin, indiceFechaFin));
			
		}catch(Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje("Error no controlado");
		}
		
		return ResponseEntity.ok(response);
	}
}
