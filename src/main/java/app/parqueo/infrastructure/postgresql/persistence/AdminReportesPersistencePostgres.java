package app.parqueo.infrastructure.postgresql.persistence;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ReporteDeParqueos;
import app.parqueo.domain.persistence_ports.AdminReportesPersistence;
import app.parqueo.infrastructure.postgresql.repositories.AdminReportesRepository;

@Repository("AdminReportesPersistence")
public class AdminReportesPersistencePostgres implements AdminReportesPersistence{

	
	private final AdminReportesRepository adminReportesRepository;
	
    @Autowired
    public AdminReportesPersistencePostgres(AdminReportesRepository adminReportesRepository) 
    {
        this.adminReportesRepository = adminReportesRepository;
    }

	@Override
	public List<ReporteDeParqueos> reporteDeParqueos(Integer pagenumber, Integer pagesize, Integer idmunicipio,
			Date fechainicio, Integer indiceFechainicio,
			Date fechafin, Integer indiceFechafin) {
		List<ReporteDeParqueos> lista = new ArrayList<>();
		
		List<Object[]> listaObjetos1 = this.adminReportesRepository.reporteDeParqueos(pagenumber, pagesize, 
				idmunicipio,fechainicio,indiceFechainicio,fechafin,indiceFechafin);
		List<Object[]> listaObjetos2 = this.adminReportesRepository.reporteDeParqueosDeudaTotal(pagenumber, pagesize, idmunicipio);

		for(int x = 0;x<listaObjetos1.size();x++) {
			ReporteDeParqueos entidad = new ReporteDeParqueos();
			entidad.setRowNumber(Integer.parseInt(listaObjetos1.get(x)[0].toString()));
			entidad.setRowCount(Integer.parseInt(listaObjetos1.get(x)[1].toString()));
			entidad.setPageCount(Integer.parseInt(listaObjetos1.get(x)[2].toString()));
			entidad.setPageIndex(Integer.parseInt(listaObjetos1.get(x)[3].toString()));		
			entidad.setZona(listaObjetos1.get(x)[4].toString());
			entidad.setCantidad_parqueos(Integer.parseInt(listaObjetos1.get(x)[5].toString()));		
			entidad.setTiempo(listaObjetos1.get(x)[6].toString());		
			entidad.setMonto((listaObjetos1.get(x)[7] == null) ? null : new BigDecimal(listaObjetos1.get(x)[7].toString()));
			entidad.setMonto_cobrado((listaObjetos1.get(x)[8] ==  null) ? null : new BigDecimal(listaObjetos1.get(x)[8].toString()));
			entidad.setPor_cobrar((listaObjetos1.get(x)[9] == null) ? null : new BigDecimal(listaObjetos1.get(x)[9].toString()));
			entidad.setDeuda_total((listaObjetos2.get(x)[4] ==  null) ? null : new BigDecimal(listaObjetos2.get(x)[4].toString()));
		
			lista.add(entidad);
		}
		return lista;
	}

}
