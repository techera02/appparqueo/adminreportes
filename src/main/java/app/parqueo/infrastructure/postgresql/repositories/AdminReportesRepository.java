package app.parqueo.infrastructure.postgresql.repositories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.MunicipioEntity;

public interface AdminReportesRepository extends JpaRepository<MunicipioEntity, Integer> {

	@Query(
	value = "SELECT * FROM \"UFN_ReporteDeParqueos\"(:pagenumber,:pagesize, :idmuni,:fechainicio,:indicefechainicio,:fechafin,:indicefechafin)"
	, nativeQuery = true)
	List<Object[]> reporteDeParqueos(
			@Param("pagenumber") Integer pagenumber, 
			@Param("pagesize") Integer pagesize,
			@Param("idmuni") Integer idmuni,
			@Param("fechainicio")Date fechainicio,
			@Param("indicefechainicio")Integer indiceFechainicio,
			@Param("fechafin")Date fechafin,
			@Param("indicefechafin")Integer indiceFechafin);
	
	@Query(value = "SELECT * FROM \"UFN_ReporteDeParqueosDeudaTotal\"(:pagenumber,:pagesize, :idmuni)", nativeQuery = true)
	List<Object[]> reporteDeParqueosDeudaTotal(@Param("pagenumber") Integer pagenumber, @Param("pagesize") Integer pagesize,
			@Param("idmuni") Integer idmuni);
}
